package com.techu.seguridad.config;

import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;

public class JwtTokenUtil implements Serializable {

    public static final long JWT_TOKEN_VALIDITY = 5*60*60;

    @Value("${jwt.secret}")
    private String secreto;


    public <T> T getClainFromToken(String token, Function<Claims, T> claimsResolver){
        final Claims claims = getAllClaimsFromToken(token);
        return clainsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.par (token);

        private Boolean isTokenExpired(String token){
            final Date expiration = getExpirationDateFromToken(token);
            return expiration.before(new Date());
        }
    }

    

}
